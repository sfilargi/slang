extern crate futures;
extern crate libc;
extern crate tokio;
extern crate tokio_core;

use std::sync::{Arc, Mutex};

use std::net::{TcpListener, TcpStream};
use std::os::unix::io::{RawFd, AsRawFd};
use std::collections::HashMap;
use std::boxed::Box;
use std::io::Read;

use futures::prelude::*;
use futures::future::{Future};
use futures::task::{current, Task};

use std::thread;

struct Waker {
    fd: RawFd,
    task: Task,
}

impl Waker {
    fn wake(&mut self) {
        println!("Waking..?");
        self.task.notify();
    }
    fn fd(&self) -> RawFd {
        self.fd
    }
}

struct Listener {
    wakers: Arc<Mutex<HashMap<RawFd, Waker>>>,
    listener: TcpListener,
    eventfd: RawFd,
}

impl Future for Listener {
    type Item = TcpStream;
    type Error = ();
    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        match self.listener.accept() {
            Ok((tcp, _)) => return Ok(Async::Ready(tcp)),
            Err(hmm) => println!("{}", hmm),
        }

        println!("Hmmm");
        self.wakers.lock().unwrap().insert(self.listener.as_raw_fd(), Waker{
            fd: self.listener.as_raw_fd(),
            task: current(),
        });
        println!("Writing to eventfd... {}", self.eventfd);
        unsafe {
            let mut i : u64 = 1;
            let x = libc::write(self.eventfd,  &mut i as *mut u64 as *mut libc::c_void, 8);
            println!("Wrote {}", x);
        }
        Ok(Async::NotReady)
    }
}

struct Reactor {
    wakers: Arc<Mutex<HashMap<RawFd, Waker>>>,
    eventfd: RawFd,
}

impl Reactor {

    fn listen(&self) -> Listener {
        let listener = TcpListener::bind("127.0.0.1:8080").unwrap();
        listener.set_nonblocking(true).unwrap();
        Listener{
            wakers: self.wakers.clone(),
            listener: listener,
            eventfd: self.eventfd,
        }
    }

    fn register(&self, w: Waker) {
        self.wakers.lock().unwrap().insert(w.fd(), w);
    }

    fn new() -> Reactor {
        let eventfd = unsafe { libc::eventfd(0, libc::EFD_NONBLOCK) };
        Reactor {
            wakers: Arc::new(Mutex::new(HashMap::new())),
            eventfd: eventfd,
        }
    }

    fn run(&mut self) {
        loop {
            let mut fds: Vec<libc::pollfd> = Vec::new();
            println!("Polling {}", self.eventfd);
            fds.push(libc::pollfd{
                fd: self.eventfd,
                events: libc::POLLIN,
                revents: 0,
            });
            for (fd, _) in self.wakers.lock().unwrap().iter()  {
                fds.push(libc::pollfd{
                    fd: *fd,
                    events: libc::POLLIN | libc::POLLOUT,
                    revents: 0,
                });
            }

            unsafe {
                libc::poll(fds.as_mut_ptr(), fds.len() as u64, -1)
            };
            println!("woke up");

            for fd in fds {
                if fd.revents != 0 {
                    if fd.fd == self.eventfd {
                        unsafe {
                            let mut i: u64 = 0;
                            libc::read(self.eventfd, &mut i as *mut u64 as *mut libc::c_void, 8);
                        }
                    }
                    match self.wakers.lock().unwrap().get_mut(&fd.fd) {
                        Some(w) => {
                            if fd.revents & libc::POLLIN != 0 {
                                w.wake();
                            }
                        }
                        None => (),
                    }
                    self.wakers.lock().unwrap().remove(&fd.fd);
                }
            }
        }
    }
}

fn main() {

    let mut reactor = Reactor::new();

    let mut l = reactor.listen();

    // let listener = Box::new(Listener {
    //     listener: TcpListener::bind("127.0.0.1:8080").unwrap()
    // });
    // listener.listener.set_nonblocking(true).unwrap();

    // let l = reactor.listen();

    // reactor.register(listener.listener.as_raw_fd(), listener);
    let wakers = reactor.wakers.clone();
    let eventfd = reactor.eventfd.clone();

    thread::spawn(move || {
        let mut r = Reactor{
            wakers: wakers,
            eventfd: eventfd,
        };
        r.run();
    });

    let mut e = tokio_core::reactor::Core::new().unwrap();
    e.run(l);
}