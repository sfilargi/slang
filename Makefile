.PHONY: build run

PWD := $(shell pwd)

run: build
	docker run -it --rm -v ${PWD}:/ws --name slang slang bash

build:
	docker build . --tag slang