FROM debian:stretch

RUN apt-get update -qq

RUN apt-get install -yqq locales
RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

RUN apt-get install -yqq procps
RUN apt-get install -yqq sudo
RUN apt-get install -yqq tmux
RUN apt-get install -yqq curl
RUN apt-get install -yqq gnupg
RUN apt-get install -yqq build-essential
RUN apt-get install -yqq telnet

RUN adduser --disabled-password --gecos '' docker
RUN adduser docker sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

ENV  LANG => en_US.utf8

USER docker

RUN curl https://sh.rustup.rs -sSf | sh -s - -y

WORKDIR /ws

ENV PATH="${PATH}:/home/docker/.cargo/bin"

